# Installation and Running Instructions

## FRONT URL

##### http://localhost:8101/web/index.html

## DATABASE SETUP

##### Connect with the MySQL/MariaDB server
~~~
sudo mysql -u root -p
~~~

##### Create DB user
~~~
create user 'inbankdb'@'%' identified by 'tere';
~~~

##### Create DB
~~~
CREATE DATABASE inbankdb CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
~~~

##### Grant privileges to the DB user
~~~
grant all privileges on inbankdb.* to 'inbankdb'@'%';
~~~

## BUILD
Navigate to the project root folder and then...
~~~
./gradlew clean build (Linux)
~~~
...or...
~~~
gradlew clean build (Windows)
~~~


## RUN APPLICATION

Navigate to the project root folder and then...
~~~
./gradlew bootRun (Linux)
~~~
...or...
~~~
gradlew bootRun (Windows)
~~~
...or...
~~~
java -jar build/libs/finance-api.jar
~~~
