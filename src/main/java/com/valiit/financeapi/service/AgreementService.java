package com.valiit.financeapi.service;

import com.valiit.financeapi.dto.ApplicationDto;
import com.valiit.financeapi.dto.GenericResponseDto;
import com.valiit.financeapi.model.Agreement;
import com.valiit.financeapi.model.Segment;
import com.valiit.financeapi.model.User;
import com.valiit.financeapi.repository.AgreementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AgreementService {

    static final double INTEREST = 10.0;
    static final double SEGMENT1 = 100.0;
    static final double SEGMENT2 = 200.0;
    static final double SEGMENT3 = 1000.0;
    static final int MAX_PERIOD = 60;

    @Autowired
    private AgreementRepository agreementRepository;

    @Autowired
    private UserService userService;

    public List<Agreement> getAgreements() {
        User currentUser = userService.getUser();
        return agreementRepository.getAgreements(currentUser.getId());
    }

    public Segment getSegment(int userId) {
        return agreementRepository.getSegment(userId);
    }

    public Agreement getAgreement(int id) {
        Assert.isTrue(id > 0, "The ID of the agreement not specified");
        return agreementRepository.getAgreement(id);
    }

    public void saveAgreement(Agreement agreement) {
        Assert.notNull(agreement, "Agreement not specified");
        Assert.hasText(agreement.getAgreementNumber(), "Agreement number not specified");
        Assert.notNull(agreement.getStartDate(), "Agreement start date not specified");
        Assert.isTrue(agreement.getStartDate() == agreement.getEndDate(), "Agreement start date cannot be end date");

        if (agreement.getId() != null && agreement.getId() > 0) {
            agreementRepository.updateAgreement(agreement);
        } else {
            Assert.isTrue(!agreementRepository.agreementExists(agreement), "The agreement with the specified number already exists");
            agreementRepository.addAgreement(agreement);
        }
    }

    public void deleteAgreement(int id) {
        if (id > 0) {
            agreementRepository.deleteAgreement(id);
        }
    }

    public GenericResponseDto processApplication(ApplicationDto application) {
        User currentUser = userService.getUser();
        Segment segment = this.getSegment(currentUser.getId());

        if(segment.getSegment().equals("debt")){
            GenericResponseDto response = new GenericResponseDto();
            response.getErrors().add("Kahjuks ei saa pakkuda Teile lisalaenu!");
            return response;
        } else {
            int clientCreditModifier = segment.getCreditModifier();
            double clientCreditScore = findCreditScore(clientCreditModifier, application.getLoanAmount(), application.getLoanPeriod());
            double responseLoan = responseToApplicationLoan(clientCreditModifier, clientCreditScore, application.getLoanAmount(), application.getLoanPeriod());
            int responsePeriod = responseToApplicationPeriod(clientCreditModifier, clientCreditScore, application.getLoanAmount(), application.getLoanPeriod());
            double responseMonthlyPayment = findMonthlyPayment(responsePeriod, responseLoan);
            System.out.println("Client credit modifier is: " + clientCreditModifier);
            System.out.println("Client credit score is: " + clientCreditScore);
            System.out.println(
                    "\n" + "Response to loan application" +
                            "\n" + "max loan amount: " + responseLoan + " eur" +
                            "\n" + "loan period: " + responsePeriod + " kuu(d)" +
                            "\n" + "interest rate: " + INTEREST + "%" +
                            "\n" + "monthly payment: " + responseMonthlyPayment + " eur"
            );
            // salvesta application
            ApplicationDto newApplication = new ApplicationDto();
            newApplication.setLoanAmount(responseLoan);
            newApplication.setLoanPeriod(responsePeriod);
            agreementRepository.addApplication(newApplication, currentUser.getId(),INTEREST, responseMonthlyPayment);
            return new GenericResponseDto();
        }
    }

    public double findCreditScore (int clientCreditModifier, double loanAmount, int loanPeriod){
        double creditScore = (double)clientCreditModifier / loanAmount * loanPeriod;
        creditScore = Math.round(creditScore*100)/100.0d;
        return creditScore;
    }

    public static int responseToApplicationLoan(int clientCreditModifier, double creditScore, double loanAmount, int loanPeriod) {
        if (clientCreditModifier == SEGMENT3) {
            int maxLoanAmount = Math.round(clientCreditModifier * MAX_PERIOD);
            if (maxLoanAmount >= 2000 && maxLoanAmount <= 10000) {
                return maxLoanAmount;
            } else if (maxLoanAmount < 2000) {
                return 2000;
            } else {
                return 10000;
            }
        } else if (clientCreditModifier == SEGMENT2) {
            int proposedLoanAmount = 0;
            if (creditScore >= 1) {
                proposedLoanAmount = Math.round(clientCreditModifier * loanPeriod);
            } else { // creditscore < 1
                proposedLoanAmount = (int)loanAmount;
            }
            if (proposedLoanAmount >= 2000 && proposedLoanAmount <= 10000) {
                return proposedLoanAmount;
            } else if (proposedLoanAmount < 2000) {
                return 2000;
            } else {
                return 10000;
            }
        } else if (clientCreditModifier == SEGMENT1) {
            int maxLoanAmount = Math.round(clientCreditModifier * MAX_PERIOD);
            int proposedLoanAmount = 0;
            if (creditScore >= 1) {
                proposedLoanAmount = Math.round(clientCreditModifier * loanPeriod);
            } else { // creditscore < 1
                if (loanAmount <= maxLoanAmount) {
                    proposedLoanAmount = (int)loanAmount;
                } else {
                    proposedLoanAmount = maxLoanAmount;
                }
            }
            if (proposedLoanAmount >= 2000 && proposedLoanAmount <= 10000) {
                return proposedLoanAmount;
            } else if (proposedLoanAmount < 2000) {
                return 2000;
            } else {
                return 10000;
            }
        } else {
            return 0;
        }
    }

    public static int responseToApplicationPeriod(int clientCreditModifier, double creditScore, double loanAmount, int loanPeriod) {
        if (clientCreditModifier == SEGMENT3) {
            return loanPeriod;
        } else if (clientCreditModifier == SEGMENT2) {
            if (creditScore >= 1) {
                return loanPeriod;
            } else { // creditScore < 1
                int minLoanPeriod = (int) ((double) loanAmount / (double) clientCreditModifier);
                if (minLoanPeriod >= 12 && minLoanPeriod <= 60) {
                    return minLoanPeriod;
                } else if (minLoanPeriod < 12) {
                    return 12;
                } else { // minLoanPeriod > 60
                    return 60;
                }
            }
        } else if (clientCreditModifier == SEGMENT1) {
            if (creditScore >= 1) {
                return loanPeriod;
            } else { // creditscore < 1
                int minLoanPeriod = (int) ((double) loanAmount / (double) clientCreditModifier);
                if (minLoanPeriod >= 12 && minLoanPeriod <= 60) {
                    return minLoanPeriod;
                } else if (minLoanPeriod < 12) {
                    return 12;
                } else { // minLoanPeriod > 60
                    return 60;
                }
            }
        } else {
            return 0;
        }
    }

    public double findMonthlyPayment(int loanPeriod, double loanAmount){
        int year = 12;
        double p = ((INTEREST/100)/year);
        double pPeriod = Math.pow((1 + p), loanPeriod);
        double monthlyPayment = Math.round(loanAmount * (p * pPeriod  / (pPeriod - 1)) * 100) / 100.0;
        return monthlyPayment;
    }
}
