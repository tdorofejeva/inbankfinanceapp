package com.valiit.financeapi.repository;

import com.valiit.financeapi.dto.ApplicationDto;
import com.valiit.financeapi.model.Agreement;
import com.valiit.financeapi.model.Segment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Repository
public class AgreementRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Agreement> getAgreements(int userId) {
        return jdbcTemplate.query("select * from agreement where user_id = ?", new Object[]{userId}, mapAgreementRows);
    }

    public Agreement getAgreement(int id) {
        List<Agreement> agreements = jdbcTemplate.query("select * from agreement where id = ?", new Object[]{id}, mapAgreementRows);
        return agreements.size() > 0 ? agreements.get(0) : null;
    }


    public Segment getSegment(int userId) {
        List<Segment> segments = jdbcTemplate.query("select * from credit_modifier where user_id = ?", new Object[]{userId}, mapSegmentRows);
        return segments.size() > 0 ? segments.get(0) : null;
    }

    public boolean agreementExists(Agreement agreement) {
        Integer count = jdbcTemplate.queryForObject("select count(id) from agreement where user_id = ?", new Object[]{agreement.getUserId()}, Integer.class);
        return count != null && count > 0;
    }

    public void addAgreement(Agreement agreement) {
        jdbcTemplate.update(
                "insert into agreement (user_id, agreement_number, start_date, loan_amount, loan_period, loan_interest, monthly_payment, status) values (?, ?, ?, ?, ?, ?, ?, ?)",
                agreement.getUserId(), agreement.getAgreementNumber(), agreement.getStartDate(), agreement.getLoanAmount(),
                agreement.getLoanPeriod(), agreement.getLoanInterest(), agreement.getMonthlyPayment(), agreement.getStatus()
        );
    }

    public void addApplication(ApplicationDto application, int userId, double interest, double monthlyPayment) {
        jdbcTemplate.update(
                "insert into agreement (user_id, agreement_number, start_date, loan_amount, loan_period, status, loan_interest, monthly_payment) values (?, ?, ?, ?, ?, ?, ?, ?)",
                userId, "Taotlus", LocalDate.now(), application.getLoanAmount(), application.getLoanPeriod(), "ootel", interest, monthlyPayment
        );
    }

    public void updateAgreement(Agreement agreement) {
        jdbcTemplate.update(
                "update agreement set user_id = ?, agreement_number = ?, start_date = ?, loan_amount = ?, loan_period = ?, loan_interest = ?, monthly_payment = ?, status = ? where id = ?",
                agreement.getUserId(), agreement.getAgreementNumber(), agreement.getStartDate(), agreement.getLoanAmount(),
                agreement.getLoanPeriod(), agreement.getLoanInterest(), agreement.getMonthlyPayment(), agreement.getStatus()
        );
    }

    public void deleteAgreement(int id) {
        jdbcTemplate.update("delete from agreement where id = ?", id);
    }

    private RowMapper<Agreement> mapAgreementRows = (rs, rowNum) -> {
        Agreement agreement = new Agreement();
        agreement.setId(rs.getInt("id"));
        agreement.setUserId(rs.getInt("user_id"));
        agreement.setAgreementNumber(rs.getString("agreement_number"));
        agreement.setStartDate(rs.getDate("start_date") != null ? rs.getDate("start_date").toLocalDate() : null);
        agreement.setEndDate(rs.getDate("start_date") != null ? rs.getDate("start_date").toLocalDate().plusMonths(rs.getInt("loan_period")) : null);
        agreement.setLoanAmount(rs.getDouble("loan_amount"));
        agreement.setLoanPeriod(rs.getInt("loan_period"));
        agreement.setLoanInterest(rs.getDouble("loan_interest"));
        agreement.setMonthlyPayment(rs.getDouble("monthly_payment"));
        agreement.setStatus(rs.getString("status"));
        return agreement;
    };

    private RowMapper<Segment> mapSegmentRows = (rs, rowNum) -> {
        Segment segment = new Segment();
        segment.setUserId(rs.getString("user_id"));
        segment.setSegment(rs.getString("segment"));
        segment.setCreditModifier(rs.getInt("credit_modifier"));
        return segment;
    };
}
