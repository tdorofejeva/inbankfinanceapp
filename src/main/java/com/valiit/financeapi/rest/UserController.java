package com.valiit.financeapi.rest;

import com.valiit.financeapi.dto.GenericResponseDto;
import com.valiit.financeapi.dto.JwtRequestDto;
import com.valiit.financeapi.dto.JwtResponseDto;
import com.valiit.financeapi.dto.UserRegistrationDto;
import com.valiit.financeapi.model.User;
import com.valiit.financeapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@CrossOrigin("*")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public GenericResponseDto register(@RequestBody UserRegistrationDto userRegistration) {
        return userService.register(userRegistration);
    }

    @PostMapping("/login")
    public JwtResponseDto authenticate(@RequestBody JwtRequestDto request) throws Exception {
        return userService.authenticate(request);
    }

    @GetMapping("/currentUser")
    public User authenticate() throws Exception {
        return userService.getUser();
    }
}