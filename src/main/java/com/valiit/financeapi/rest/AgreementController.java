package com.valiit.financeapi.rest;

import com.valiit.financeapi.dto.ApplicationDto;
import com.valiit.financeapi.dto.GenericResponseDto;
import com.valiit.financeapi.model.Agreement;
import com.valiit.financeapi.service.AgreementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/agreements")
@CrossOrigin("*")
public class AgreementController {

    @Autowired
    private AgreementService agreementService;

    @GetMapping
    public List<Agreement> getAgreements() {
        return agreementService.getAgreements();
    }

    @GetMapping("/{id}")
    public Agreement getAgreement(@PathVariable("id") int id) {
        return agreementService.getAgreement(id);
    }

    @PostMapping
    public void saveAgreement(@RequestBody Agreement agreement) {
        agreementService.saveAgreement(agreement);
    }


    @DeleteMapping("/{id}")
    public void deleteAgreement(@PathVariable("id") int id) {
        agreementService.deleteAgreement(id);
    }

    @PostMapping("/application")
    public GenericResponseDto processApplication(@RequestBody ApplicationDto application) {
        GenericResponseDto response = agreementService.processApplication(application);
        return response;
    }


}
