package com.valiit.financeapi.model;

public class Segment {
    private String userId;
    private String segment;
    private int creditModifier;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public int getCreditModifier() {
        return creditModifier;
    }

    public void setCreditModifier(int creditModifier) {
        this.creditModifier = creditModifier;
    }

    @Override
    public String toString() {
        return "Segment{" +
                "clientId='" + userId + '\'' +
                ", segment='" + segment + '\'' +
                ", creditModifier=" + creditModifier +
                '}';
    }
}
