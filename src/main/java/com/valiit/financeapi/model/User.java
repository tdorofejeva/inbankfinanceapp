package com.valiit.financeapi.model;

public class User {

    private int id;
    private String username;
    private String password;
    private String userRole; // change Repository!
    private String firstName;
    private String lastName;
    private String personalId;
    private String email;

    public User(int id, String username, String password, String userRole,
                String firstName, String lastName, String personalId, String email) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.userRole = userRole;
        this.firstName = firstName;
        this.lastName = lastName;
        this.personalId = personalId;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserRole() { return userRole; }

    public String getFirstName() { return firstName; }

    public String getLastName() { return lastName; }

    public String getPersonalId() { return personalId; }

    public String getEmail() { return email; }


    public void setUserRole(String userRole) { this.userRole = userRole; }

    public void setFirstName(String firstName) { this.firstName = firstName; }

    public void setLastName(String lastName) { this.lastName = lastName; }

    public void setPersonalId(String personalId) { this.personalId = personalId; }

    public void setEmail(String email) { this.email = email; }
}
