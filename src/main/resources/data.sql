INSERT INTO `user` (`id`, `username`, `password`, `user_role`, `first_name`, `last_name`, `personal_id`, `email`) VALUES
	(1, 'admin', '$2a$10$iz1QiuSVUMFcXdx5Xs2mr.OdTZBuutK0GrKqWS8x2OhaDz8jKV5ei', '', '', '', '', ''),
	(10, 'tatjana', '$2a$10$1Dngzct2ZDd1kRu8Wjla1.pu6NQ4Q.MKIVEG5TC5eSLXttC1H0M.y', 'user', 'Tatjana', 'Dorofejeva', '48302210325', 'tdorofejeva@gmail.com'),
	(13, 'segment.debt', '$2a$10$pANrJVcgH63PktbhkH1veuQVrAR4Tpkt9homjaRkk7vUtRjRkUDdK', 'user', 'Segment debt', 'Qwertyuiop', '37901270226', 'segment.debt@gmail.com'),
	(14, 'segment1', '$2a$10$51LW6bu7EetwUvZadPrbI.YFa04T2ax2s96dmgxhc2Thq6Jee4zBG', 'user', 'Segment1', 'CreditModifier100', '35906240226', 'segment1@gmail.com'),
	(15, 'segment2', '$2a$10$xeg0MdHhrg4U.YCdbs6Z5eckHiO8Yqpxza3JZFEfn6VLF6oBbqbwG', 'user', 'Segment2', 'CreditModifier200', '48706190224', 'segment2@gmail.com'),
	(16, 'segment3', '$2a$10$MvWDgYumnhxEO5.ZjeSmBOc9aze.MyMaSWBqX/3dIOW97J6Ugkf8C', 'user', 'Segment3', 'CreditModifier1000', '49211250335', 'segment3@gmail.com');

INSERT INTO `credit_modifier` (`id`, `user_id`, `segment`, `credit_modifier`) VALUES
	(1, 13, 'debt', 0),
	(3, 14, 'segment1', 100),
	(4, 15, 'segment2', 200),
	(5, 16, 'segment3', 1000);

