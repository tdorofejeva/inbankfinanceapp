let currentUser = null;


// Initialization after the HTML document has been loaded...
window.addEventListener('DOMContentLoaded', () => {
    doLoadItems();
    doLoadCurrentUser();
    displaySessionBox();
});
/*vbdjhvbdajk*/

/* 
    --------------------------------------------
    ACTION FUNCTIONS
    --------------------------------------------
*/

async function doLogin() {
    try {
        const credentials = {
            username: document.querySelector('#loginUsername').value,
            password: document.querySelector('#loginPassword').value,
        };
        const session = await login(credentials);
        storeAuthentication(session);
        displaySessionBox();
        doLoadCurrentUser();
        closePopup();
        doLoadItems();
    } catch (e) {
        handleError(e, displayLoginPopup);
    }
}

async function doRegister() {
    let errors = validateRegistrationForm();
    if (errors.length == 0) {
        try {
            const user = {
                firstName: document.querySelector('#registerFirstName').value,
                lastName: document.querySelector('#registerLastName').value,
                personalId: document.querySelector('#registerPersonalId').value,
                email: document.querySelector('#registerEmail').value,
                username: document.querySelector('#registerUsername').value,
                password: document.querySelector('#registerPassword').value,

            };
            let response = await register(user);
            if (response.errors.length == 0) {
                document.querySelector("#registrationFormDiv").style.display = "none";
                document.querySelector("#registrationSuccessDiv").style.display = "block";
            } else {
                displayRegisterErrors(response.errors);
            }
        } catch (e) {
            document.querySelector("#registrationSuccessDiv").style.display = "none";
        }
    } else {
        displayRegisterErrors(errors);
    }

}

function doLogout() {
    clearAuthentication();
    displayLoginPopup();
}

async function doLoadCurrentUser() {
    currentUser = await fetchUser();
    currentUserName = currentUser.firstName + ' ' + currentUser.lastName;
    document.querySelector('#headerLeftPanel').innerText = currentUserName;
}

async function doLoadItems() {
    try {
        const items = await fetchAgreements();
        displayItems(items);
        displayWrapper();
    } catch (e) {
        handleError(e, displayLoginPopup);
    }
}

async function doPostApplication() {
    let errors = validateApplicationForm();
    if (errors.length == 0) {
        try {
            // Reading the application data from the application form...
            let item = {
                loanAmount: document.querySelector('#itemEditLoanAmount').value,
                loanPeriod: document.querySelector('#itemEditLoanPeriod').value,
            };

            let response = await postApplication(item);
            if (response.errors.length == 0) {
                document.querySelector("#applicationFormDiv").style.display = "none";
                document.querySelector("#applyingSuccessDiv").style.display = "block";
            } else {
                displayApplicationErrors(response.errors);
            }
        } catch (e) {
            document.querySelector("#applyingSuccessDiv").style.display = "none";
        }
    } else {
        displayApplicationErrors(errors);
    }

    //     if (validateItem(item)) {
    //         // Saving the application...
    //         await postApplication(item);
    //         closePopup();
    //         doLoadItems();
    //     }
    // } catch (e) {
    //     handleError(e, displayLoginPopup);
    // }
}

async function doDeleteItem(id) {
    if (confirm('Soovid sa tõesti seda ettevõtet kustutada?')) {
        await deleteAgreement(id);
        doLoadItems();
    }
}

async function doUploadFile() {
    const logoFile = document.querySelector('#file').files[0];
    if (logoFile) {
        let uploadResponse = await postFile(logoFile);
        document.querySelector('#itemEditLogoImage').src = uploadResponse.url;
        document.querySelector('#itemEditLogo').value = uploadResponse.url;
    }
}

async function doLoadChart() {
    try {
        const items = await fetchAgreements();
        displayChart(items);
        displayWrapper();
    } catch (e) {
        handleError(e, displayLoginPopup);
    }
}

/* 
    --------------------------------------------
    DISPLAY FUNCTIONS
    --------------------------------------------
*/

function displayLoginPopup() {
    document.querySelector('#wrapper').style.display = 'none';
    openPopup(POPUP_CONF_BLANK_300_300, 'loginFormTemplate');
}

function displayRegisterPopup() {
    document.querySelector('#wrapper').style.display = 'none';
    openPopup(POPUP_CONF_BLANK_600_600, 'registerFormTemplate');
}

function displaySessionBox() {
    const sessionBox = document.querySelector('header .header-cell-right');
    if (!isEmpty(getToken())) {
        sessionBox.innerHTML = /*html*/`<strong>${getUsername()}</strong> | <a href="javascript:doLogout()">logi välja</a>`;
    }
}

async function displayItemEditPopup(id) {
    await openPopup(POPUP_CONF_DEFAULT, 'itemEditFormTemplate');

    // Clearing the possible previous errors...
    displayErrorBox([]);

    // Clearing the agreement edit form from previous data...
    document.querySelector('#itemEditId').value = '';
    document.querySelector('#itemEditFirstName').value = currentUser.firstName;
    document.querySelector('#itemEditLastName').value = currentUser.lastName;
    document.querySelector('#itemEditPersonalId').value = currentUser.personalId;
    document.querySelector('#itemEditLoanAmount').value = '';
    document.querySelector('#itemEditLoanPeriod').value = '';

    // Loading the agreement, if id specified...
    if (id > 0) {
        let item = null;
        try {
            item = await fetchAgreements(id);
        } catch (e) {
            handleError(e, displayLoginPopup);
        }

        // Filling the agreement edit form...
        document.querySelector('#itemEditId').value = item.id;
        document.querySelector('#itemEditFirstName').value = item.firstName;
        document.querySelector('#itemEditLastName').value = item.lastName;
        document.querySelector('#itemEditPersonalId').value = item.personalId;
        document.querySelector('#itemEditLoanAmount').value = item.loanAmount;
        document.querySelector('#itemEditLoanPeriod').value = item.loanPeriod;
    }
}

function displayWrapper() {
    document.querySelector('#wrapper').style.display = 'grid';
}

function displayItems(items) {
    let itemsHtml = /*html*/`
        <div class="items">
            <div class="item-fluid">
                <div class="item-100-center">
                    <h1>Minu pank</h1>
                </div>
            </div>

            <div class="item-fluid">
                <div class="item-100-center">
                    <h2 class="item-100">Lepingud</h2>
                 
                </div>
            </div>
        </div>
        <section class="items">

            <article class="item">
            <table class="table">
                <thead class="tablehead">
                    <tr> 
                        <th><div class="item-50-bold">Lepingu nr</div></th>
                        <th><div class="item-50-bold">Alguskuupäev</div></th>
                        <th><div class="item-50-bold">Lõppkuupäev</div></th>
                        <th><div class="item-50-bold">Laenusumma</div></th>
                        <th><div class="item-50-bold">Lepingu kestus</div></th>
                        <th><div class="item-50-bold">Intress</div></th>
                        <th><div class="item-50-bold">Kuumakse</div></th>
                        <th><div class="item-50-bold">Lepingu staatus</div></th>
                    </tr>
                </thead> 
    
                <tbody class="tablebody">

    `;
    for (let i = 0; i < items.length; i++) {
        itemsHtml = itemsHtml + displayItem(items[i]);
    }
    itemsHtml = itemsHtml + /*html*/`

                </tbody>
                </table>
            </article>

        </section>
        <div class="items">
            <div class="item-fluid">
                <div class="item-100-center">
                    <button onclick="displayItemEditPopup()">Taotle laenu</button>
                </div>
            </div>
        </div>
    `;
    document.querySelector('main').innerHTML = itemsHtml;
}

function displayItem(item) {
    let itemHtml = /*html*/`

                        <tr> 
                            <td> <div class="item-50" style="${item.agreementNumber === 'Taotlus' ? 'color: rgb(240, 181, 0)' : 'color: black'}">${(item.agreementNumber)}</div></td> 
                            <td> <div class="item-50">${item.startDate}</div></td> 
                            <td> <div class="item-50">${item.endDate}</div></td> 
                            <td> <div class="item-50">${formatNumber(item.loanAmount)} &euro;</div></td> 
                            <td> <div class="item-50">${formatNumber(item.loanPeriod)} kuud</div></td> 
                            <td> <div class="item-50">${formatNumber(item.loanInterest)}%</div></td> 
                            <td> <div class="item-50">${formatNumber(item.monthlyPayment)} &euro;</div></td> 
                            <td> <div class="item-50" style="${item.status === 'kehtiv' ? 'color: green' : 'color: rgb(240, 181, 0)'}">${(item.status)}</div></td> 
                        </tr>         

        `;
    return itemHtml;
}

function displayErrorBox(errors) {
    const itemEditFormContainer = document.querySelector('#itemEditFormContainer');
    const errorBox = document.querySelector('#errorBox');
    if (errors.length > 0) {
        errorBox.style.display = 'block';

        let errorsHtml = /*html*/`<div><strong>Palun paranda järgmised vead:</strong></div>`;
        for (let i = 0; i < errors.length; i++) {
            errorsHtml = errorsHtml + /*html*/`
                <div>
                    ${errors[i]}
                </div>
            `;
        }

        errorBox.innerHTML = errorsHtml;
        itemEditFormContainer.scrollTop = 0;
    } else {
        errorBox.innerHTML = '';
    }
}

function displayChart(items) {
    let chartHtml = /*html*/`
        <div class="items">
            <div class="item-fluid">
                <div class="item-100-center">
                    <h1>Börsiettevõtted</h1>
                </div>
            </div>
            <div class="item-fluid">
                <div class="item-100-center">
                    <button id="displayModeButton" onclick="doLoadItems()">Näita nimekirja</button>
                </div>
            </div>
            <div class="item-fluid">
                <canvas id="chartCanvas" style="height:40vh; width:80%"></canvas>
            </div>
        </div>
    `;
    document.querySelector('main').innerHTML = chartHtml;

    let ctx = document.querySelector('#chartCanvas').getContext('2d');
    let chartData = composeChartData(items);
    new Chart(ctx, {
        type: 'pie',
        data: chartData,
        options: {}
    });
}

/* 
    --------------------------------------------
    VALIDATION FUNCTIONS
    --------------------------------------------
*/


function validateRegistrationForm() {
    let errors = [];
    if (document.querySelector('#registerFirstName').value == null ||
        document.querySelector('#registerFirstName').value.length < 2 ||
        document.querySelector('#registerFirstName').value.length > 100) {
        errors.push('Eesnimi puudu või vale pikkusega!');
    }

    if (document.querySelector('#registerLastName').value == null ||
        document.querySelector('#registerLastName').value.length < 2 ||
        document.querySelector('#registerLastName').value.length > 100) {
        errors.push('Perekonnanimi puudu või vale pikkusega!');
    }

    if (document.querySelector('#registerPersonalId').value == null ||
        document.querySelector('#registerPersonalId').value.length != 11) {
        errors.push('Isikukood puudu või vale pikkusega!');
    }

    if (document.querySelector('#registerEmail').value == null ||
        document.querySelector('#registerEmail').value.length < 6) {
        errors.push('E-posti aadress puudu või vale pikkusega!');
    }

    if (isEmail(document.querySelector('#registerEmail').value) != true) {
        errors.push('E-posti aadress ei ole korrektne!')

    }

    if (document.querySelector('#registerUsername').value == null ||
        document.querySelector('#registerUsername').value.length < 2 ||
        document.querySelector('#registerUsername').value.length > 20) {
        errors.push('Kasutajanimi puudu või vale pikkusega!');
    }

    if (document.querySelector('#registerPassword').value == null ||
        document.querySelector('#registerPassword').value.length < 8 ||
        document.querySelector('#registerPassword').value.length > 20) {
        errors.push('Parool puudu või vale pikkusega!');
    }
    return errors;
}

function validateApplicationForm() {
    let errors = [];
    if (document.querySelector('#itemEditLoanAmount').value == null ||
        document.querySelector('#itemEditLoanAmount').value < 2000 ||
        document.querySelector('#itemEditLoanAmount').value > 10000) {
        errors.push('Laenusumma peab olema vähemalt 2000 eurot ja maksimaalselt 10000 eurot!');
    }

    if (document.querySelector('#itemEditLoanPeriod').value == null ||
        document.querySelector('#itemEditLoanPeriod').value > 60 ||
        document.querySelector('#itemEditLoanPeriod').value < 12) {
        errors.push('Laenuperiood peab olema vähemalt 12 kuud ja maksimaalselt 60 kuud!');
    }
    return errors;
}

//validation
function displayRegisterErrors(errors) {
    if (errors.length > 0) {
        let errorsHtml = '';
        for (let i = 0; i < errors.length; i++) {
            errorsHtml = errorsHtml + `<div>${errors[i]}</div>`;
        }
        document.querySelector('#registerFormContainer .errorBox').innerHTML = errorsHtml;
        document.querySelector('#registerFormContainer .errorBox').style.display = 'block';
    } else {
        document.querySelector('#registerFormContainer .errorBox').style.display = 'none';
    }
}

function displayApplicationErrors(errors2) {
    if (errors2.length > 0) {
        let errors2Html = '';
        for (let i = 0; i < errors2.length; i++) {
            errors2Html = errors2Html + `<div>${errors2[i]}</div>`;
        }
        document.querySelector('#itemEditFormContainer #errorBox').innerHTML = errors2Html;
        document.querySelector('#itemEditFormContainer #errorBox').style.display = 'block';
    } else {
        document.querySelector('#itemEditFormContainer #errorBox').style.display = 'none';
    }
}


// function validateItem(item) {
//     let errors = [];

//     if (item.loanAmount < 2000 && item.loanAmount > 10000) {
//         errors.push('Laenusumma peab olema vähemalt 2000 eurot ja maksimaalselt 10000 eurot!');
//     }

//     if (item.loanAmount < 12 && item.loanAmount > 60) {
//         errors.push('Laenuperiood peab olema vähemalt 12 kuud ja maksimaalselt 60 kuud!');
//     }

//     displayErrorBox(errors);
//     return errors == 0;
// }



/* 
    --------------------------------------------
    CHART FUNCTIONS
    --------------------------------------------
*/

function generateRandomColor() {
    let r = Math.floor(Math.random() * 255);
    let g = Math.floor(Math.random() * 255);
    let b = Math.floor(Math.random() * 255);
    return `rgb(${r},${g},${b})`;
}

function composeChartDataset(items) {
    if (items != null && items.length > 0) {
        let data = items.map(item => Math.round(item.marketCapitalization));
        let backgroundColors = items.map(generateRandomColor);
        return [{
            data: data,
            backgroundColor: backgroundColors,
        }];
    }
    return [];
}

function composeChartLabels(items) {
    return items != null && items.length > 0 ? items.map(item => item.name) : [];
}

function composeChartData(agreements) {
    return {
        datasets: composeChartDataset(agreements),
        labels: composeChartLabels(agreements)
    };
}
