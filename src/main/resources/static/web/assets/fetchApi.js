
async function login(credentials) {
    let response = await fetch(
        `${API_URL}/users/login`,
        {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(credentials)
        }
    );
    return await handleJsonResponse(response);
}

async function register(user) {
    let response = await fetch(
        `${API_URL}/users/register`,
        {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(user)
        }
    );
    return await handleJsonResponse(response);
}


async function fetchAgreements() {
    let response = await fetch(
        `${API_URL}/agreements`,
        {
            method: 'GET',
            headers: { 'Authorization': `Bearer ${getToken()}` }
        }
    );
    return await handleJsonResponse(response);
}

async function fetchUser() {
    let response = await fetch(
        `${API_URL}/users/currentUser`,
        {
            method: 'GET',
            headers: { 'Authorization': `Bearer ${getToken()}` }
        }
    );
    return await handleJsonResponse(response);
}

async function fetchAgreement(id) {
    let response = await fetch(
        `${API_URL}/agreements/${id}`,
        {
            method: 'GET',
            headers: { 'Authorization': `Bearer ${getToken()}` }
        }
    );
    return await handleJsonResponse(response);
}

async function postAgreement(agreement) {
    let response = await fetch(
        `${API_URL}/agreements`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(agreement)
        }
    );
    handleResponse(response);
}

async function postApplication(item) {
    let response = await fetch(
        `${API_URL}/agreements/application`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(item)
        }
    );
    return handleJsonResponse(response);
}

async function deleteAgreement(id) {
    let response = await fetch(
        `${API_URL}/agreements/${id}`,
        {
            method: 'DELETE',
            headers: { 'Authorization': `Bearer ${getToken()}` }
        }
    );
    handleResponse(response);
}

async function postFile(file) {
    let formData = new FormData();
    formData.append("file", file);

    let response = await fetch(
        `${API_URL}/files/upload`,
        {
            method: 'POST',
            headers: { 'Authorization': `Bearer ${getToken()}` },
            body: formData
        }
    );
    return await handleJsonResponse(response);
}

