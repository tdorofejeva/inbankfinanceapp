DROP TABLE IF EXISTS `agreement`;
DROP TABLE IF EXISTS `credit_modifier`;
DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
`id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_role` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `first_name` varchar(190) CHARACTER SET utf8mb4 NOT NULL,
  `last_name` varchar(190) CHARACTER SET utf8mb4 NOT NULL,
  `personal_id` varchar(11) CHARACTER SET utf8mb4 NOT NULL,
  `email` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`));


CREATE TABLE IF NOT EXISTS `agreement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `agreement_number` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `loan_amount` double NOT NULL,
  `loan_period` int(11) NOT NULL,
  `loan_interest` double DEFAULT NULL,
  `monthly_payment` double DEFAULT NULL,
  `status` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_agreement_user` (`user_id`),
  CONSTRAINT `FK_agreement_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`));

  CREATE TABLE IF NOT EXISTS `credit_modifier` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `user_id` int(11) NOT NULL,
    `segment` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
    `credit_modifier` int(10) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `FK_credit_modifier_user` (`user_id`),
    CONSTRAINT `FK_credit_modifier_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`));